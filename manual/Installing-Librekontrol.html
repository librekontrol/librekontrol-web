<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Librekontrol - A programmable controller editor (version
0.2, updated 3 April 2020).


Copyright (C) 2019, 2020  Brandon Invergo

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

A copy of the license is also available from the Free Software
Foundation Web site at https://www.gnu.org/licenses/fdl.html.


The document was typeset with
http://www.texinfo.org/ (GNU Texinfo).
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Installing Librekontrol (Librekontrol)</title>

<meta name="description" content="Installing Librekontrol (Librekontrol)">
<meta name="keywords" content="Installing Librekontrol (Librekontrol)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Getting-Started.html" rel="up" title="Getting Started">
<link href="Tutorial.html" rel="next" title="Tutorial">
<link href="Getting-Started.html" rel="prev" title="Getting Started">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../css/main.css">


</head>

<body lang="en">
<span id="Installing-Librekontrol"></span><div class="header">
<p>
Next: <a href="Tutorial.html" accesskey="n" rel="next">Tutorial</a>, Up: <a href="Getting-Started.html" accesskey="u" rel="up">Getting Started</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Installing-Librekontrol-1"></span><h3 class="section">2.1 Installing Librekontrol</h3>

<span id="index-Installation"></span>
<span id="index-Installing"></span>

<p>Librekontrol is available exclusively for GNU/Linux systems.  It is
dependent on device definitions in the kernel Linux and its sound
subsystem.  Thus, it is not portable to other systems.
</p>
<span id="index-Dependencies"></span>
<p>Running Librekontrol requires the following software:
</p>
<ul>
<li> GNU Guile, version 2.2 or later

</li><li> ALSA development libraries (i.e. <code>alsa-lib</code>, however the
name might vary depending on your GNU/Linux distribution)

</li><li> libevdev
</li></ul>

<p>In addition, the following software is required to build Librekontrol
from source:
</p>
<ul>
<li> GNU Make

</li><li> pkg-config
</li></ul>

<p>If installing Librekontrol from source, simply follow the standard GNU
method for installing software:
</p>
<ol>
<li> Unpack the source archive:
&lsquo;<samp>tar -xzf librekontrol-0.2.tar.gz</samp>&rsquo;.
</li><li> Navigate to the source directory:
&lsquo;<samp>cd librekontrol-0.2</samp>&rsquo;.
</li><li> Run the <samp>configure</samp> script: &lsquo;<samp>./configure</samp>&rsquo;. Run
&lsquo;<samp>./configure --help</samp>&rsquo; to view configuration options, including
setting the installation directory via <samp>--prefix</samp>.
</li><li> Compile the software: &lsquo;<samp>make</samp>&rsquo;.
</li><li> Install the software: &lsquo;<samp>make install</samp>&rsquo;.  
</li></ol>

<span id="index-Permissions"></span>
<span id="index-Reading-input-events"></span>
<span id="index-Running-Librekontrol-as-a-normal-user"></span>
<p>Finally, Librekontrol requires that the user has permission to
read and write to input devices.  The following steps require
administrator access to the system.
</p>
<p>Each input device is defined by a file-like interface in the
<samp>/dev/input</samp> directory.  Check the owner/group of the input
device file interface:
</p>
<div class="example">
<pre class="example">$ ls -l /dev/input
total 0K
...
crw-rw---- 1 root input 13, 64 Mar 20 21:24 event0
crw-rw---- 1 root input 13, 65 Mar 20 21:24 event1
...
</pre></div>

<p>Here we see that files are owned by root and belong to the
<code>input</code> group.  In order to access the input files, add your user
to the appropriate group.  For example, &lsquo;<samp>gpasswd -a myuser
input</samp>&rsquo;.  If, for whatever reason, your <samp>/dev/input</samp> directory
looks different, you might have to give user input-event permissions
via udev rules
</p>
<p>Finally, you need to a) be sure that the &ldquo;uinput&rdquo; kernel module is
loaded (e.g. by running &lsquo;<samp>modprobe uinput</samp>&rsquo;) and b) create a udev
rule to give users in the <code>input</code> group permission to read and
write from the <samp>/dev/uinput</samp> device:
</p>
<p>Create a file at <samp>/etc/udev/rules.d/99-librekontrol.rules</samp> that contains:
</p><div class="example">
<pre class="example">KERNEL==&quot;uinput&quot;, GROUP=&quot;input&quot;, MODE=&quot;0660&quot;
</pre></div>
<p>After you reboot your system, you should have full access to input
events (including &ldquo;virtual&rdquo; events used in button remapping in
Librekontrol).
</p>
<p><strong>Caution:</strong> Do not run Librekontrol with elevated privileges
(i.e. as root).  It can run arbitrary Scheme code, which is a bad idea
to do as a superuser without good reason.  You should use the group
method described above to avoid the need to run the software as root.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Tutorial.html" accesskey="n" rel="next">Tutorial</a>, Up: <a href="Getting-Started.html" accesskey="u" rel="up">Getting Started</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
