<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Librekontrol - A programmable controller editor (version
0.2, updated 3 April 2020).


Copyright (C) 2019, 2020  Brandon Invergo

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

A copy of the license is also available from the Free Software
Foundation Web site at https://www.gnu.org/licenses/fdl.html.


The document was typeset with
http://www.texinfo.org/ (GNU Texinfo).
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Defining Devices (Librekontrol)</title>

<meta name="description" content="Defining Devices (Librekontrol)">
<meta name="keywords" content="Defining Devices (Librekontrol)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="High_002dLevel-Configuration.html" rel="up" title="High-Level Configuration">
<link href="Callbacks.html" rel="next" title="Callbacks">
<link href="High_002dLevel-Configuration.html" rel="prev" title="High-Level Configuration">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../css/main.css">


</head>

<body lang="en">
<span id="Defining-Devices"></span><div class="header">
<p>
Next: <a href="Callbacks.html" accesskey="n" rel="next">Callbacks</a>, Up: <a href="High_002dLevel-Configuration.html" accesskey="u" rel="up">High-Level Configuration</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Defining-Devices-1"></span><h4 class="subsection">5.1.1 Defining Devices</h4>

<span id="index-Devices-2"></span>
<p>These procedures are used to configure devices.  This generally
involves defining the controls on the device and then calling the
high-level configuration function <var>define-device</var>.  The procedures
in this function are contained in the module &lsquo;<samp>(librekontrol
device)</samp>&rsquo;.
</p>
<span id="index-Controls-2"></span>
<dl>
<dt id="index-control">Record type: <strong>control</strong> <em>input-event alsa-ctl</em></dt>
<dd><p>The <var>control</var> record type contains information about a control.  A
control (e.g. a button) is associated with a particular
<var>input-event</var> (the event it sends when the user manipulates the
control) and it may also have an associated <var>alsa-ctl</var> (which
controls an LED on the device, as defined by ALSA).  If there is no
ALSA ctl, <var>alsa-ctl</var> will be &lsquo;<samp>#f</samp>&rsquo;.
</p></dd></dl>

<dl>
<dt id="index-make_002dcontrol">Function: <strong>make-control</strong> <em>input-event alsa-ctl</em></dt>
<dd><p>Make a new <var>control</var> record combining <var>input-event</var> and
<var>alsa-ctl</var>.
</p></dd></dl>

<dl>
<dt id="index-control_003f">Function: <strong>control?</strong> <em>x</em></dt>
<dd><p>Return &lsquo;<samp>#t</samp>&rsquo; if <var>x</var> is a <var>control</var> record
</p></dd></dl>

<dl>
<dt id="index-input_002devent">Function: <strong>input-event</strong> <em>control</em></dt>
<dt id="index-alsa_002dctl">Function: <strong>alsa-ctl</strong> <em>control</em></dt>
<dd><p>Return the input event or ALSA ctl handle associated with
<var>control</var>.
</p></dd></dl>

<dl>
<dt id="index-define_002dcontrol">Syntax: <strong>define-control</strong> <em>symbol input-event alsa-ctl</em></dt>
<dd><p>Make a control record for <var>input-event</var> and <var>alsa-ctl</var> and
bind it to <var>symbol</var>.
</p></dd></dl>

<dl>
<dt id="index-make_002dinput_002dmax_002dparameter">Function: <strong>make-input-max-parameter</strong> <em>max-value</em></dt>
<dd><p>Parameterize <var>max-value</var>, ensuring that any new values assigned to
it are greater than zero but less-than or equal to <var>max-value</var>.
This is for defining the maximum value that an input event can take
(which should be set in the device&rsquo;s driver code in the kernel).
</p></dd></dl>

<span id="index-Callback-functions-1"></span>
<dl>
<dt id="index-connect_002dcontrol">Function: <strong>connect-control</strong> <em>device control callback [keyword option ...]</em></dt>
<dd><p>Connect <var>control</var> on <var>device</var> to the function <var>callback</var>.
Whenever <var>control</var> is activated (i.e. Librekontrol detects its
associated input event), <var>callback</var> is called.
</p>
<p><var>callback</var> must be a procedure that takes the following arguments:
<var>device</var>, <var>input-event</var>, <var>input-value</var>, <var>alsa-ctl</var>.
It may also take any number of optional keyword arguments.  The
optional keyword arguments and their values must be specified in the
call to <var>make-input-max-parameter</var> as <var>keyword</var> and
<var>option</var>.
</p>
<p>For example, to connect a device to the <var>midi-note-button</var>
callback:
</p>
<div class="example">
<pre class="example">(connect-control my-device button 'midi-note-button #:channel 1 #:note
  'midi-note-c3 #:velocity 63)
</pre></div>

<p>These optional arguments will then be passed to the
<var>midi-note-button</var> procedure each time <var>button</var> is pressed.
</p></dd></dl>

<dl>
<dt id="index-configure_002ddevice">Function: <strong>configure-device</strong> <em>[alsa-name input-name open-midi-seq create-input-remapper        remap-events controls idle-wait idle-hooks exit-hooks init]</em></dt>
<dd>
<p>Open a device using ALSA device <var>alsa-name</var> and/or input device
<var>input-name</var>.
</p>
<p>If <var>open-midi-seq</var> is &lsquo;<samp>#t</samp>&rsquo; open an ALSA MIDI sequencer for
the device (which will be called &ldquo;Librekontrol &lt;alsa-name&gt;
read/write&rdquo;).  If <var>create-input-remapper</var> is &lsquo;<samp>#t</samp>&rsquo;, an input
remapper is created for the device, which is required to send new
input events.  Both <var>open-midi-seq</var> and
<var>create-input-remapper</var> default to &lsquo;<samp>#t</samp>&rsquo; if they are not
specified.
</p>
<p>The device is further configured through additional keyword arguments.
An argument that is not present or set to &lsquo;<samp>#f</samp>&rsquo; is ignored.
</p>
<p>Any &ldquo;virtual&rdquo; input events to be generated by the device through
event-remapping must first be listed in <var>remap-events</var>.  The
argument must consist of a list of events created using the
<var>make-input-event</var> procedure.  See <a href="Input-Events.html">Input Events</a>.
</p>
<p>Device controls and their connection to callbacks are defined in
<var>controls</var>.  This argument must contain a list, where each item is
a control specification.  Each control specification is a list
containing a control (created via <var>make-control</var>), a callback
function, and any keyword arguments to be passed to the callback
function.
</p>
<span id="index-Idle-hooks"></span>
<p>The <var>idle-hooks</var> argument consists of a list of procedures that
take no arguments.  These procedures will be called every
<var>idle-wait</var> seconds (1 second if not specified).
</p>
<span id="index-Exit-hooks-1"></span>
<p><var>exit-hooks</var> must contain a list of procedures that take no
arguments.  These procedures are to be run when Librekontrol exits.
</p>
<p>Additional initialization procedures can be listed in the <var>init</var>
argument.  As with other hooks, these must not take any arguments.
</p>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Callbacks.html" accesskey="n" rel="next">Callbacks</a>, Up: <a href="High_002dLevel-Configuration.html" accesskey="u" rel="up">High-Level Configuration</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
