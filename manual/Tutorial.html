<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Librekontrol - A programmable controller editor (version
0.2, updated 3 April 2020).


Copyright (C) 2019, 2020  Brandon Invergo

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

A copy of the license is also available from the Free Software
Foundation Web site at https://www.gnu.org/licenses/fdl.html.


The document was typeset with
http://www.texinfo.org/ (GNU Texinfo).
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Tutorial (Librekontrol)</title>

<meta name="description" content="Tutorial (Librekontrol)">
<meta name="keywords" content="Tutorial (Librekontrol)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Getting-Started.html" rel="up" title="Getting Started">
<link href="Example-Config-Modules.html" rel="next" title="Example Config Modules">
<link href="Installing-Librekontrol.html" rel="prev" title="Installing Librekontrol">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../css/main.css">


</head>

<body lang="en">
<span id="Tutorial"></span><div class="header">
<p>
Previous: <a href="Installing-Librekontrol.html" accesskey="p" rel="prev">Installing Librekontrol</a>, Up: <a href="Getting-Started.html" accesskey="u" rel="up">Getting Started</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Tutorial-1"></span><h3 class="section">2.2 Tutorial</h3>

<span id="index-Tutorial"></span>
<p>The best way to introduce Librekontrol is to dive right in with an
example.
</p>
<p>Here we will be configuring the Audio Kontrol 1 (<abbr>AK1</abbr>) USB sound
card from Native Instruments.  This device has several knobs and
buttons on its face that directly control the internal electronics.
The top of the device also has three buttons and a large knob, which
send input events to the computer.
</p>
<p>By default, these controls aren&rsquo;t very useful.  The three buttons
(let&rsquo;s call them &ldquo;left&rdquo;, &ldquo;middle&rdquo;, and &ldquo;right&rdquo;) send the
<kbd>a</kbd>, <kbd>b</kbd>, and <kbd>c</kbd> keyboard events, respectively.  That is,
if you have a text editor open and you press the left button, the
letter &lsquo;<samp>a</samp>&rsquo; would be typed.  Similarly, turning the knob results
in the mouse cursor moving left and right<a id="DOCF2" href="#FOOT2"><sup>2</sup></a>.
</p>
<p>Furthermore, each of these four controls has an associated LED under
them that should light up when the control is used, however this does
not happen on its own.
</p>
<p>Let&rsquo;s say that we want to use the AK1 to control the audio software
Ardour.  We want the left and right buttons to move the current
playing position to the start and the end of the song, respectively.
We want the middle button to toggle the current playing state
(play/pause).  Finally, we want the big knob to scroll the editor
window left and right.
</p>
<p>Ardour allows all of these functions to be mapped to whatever key you
want, so theoretically we could just map the above functions to the
<kbd>a</kbd>, <kbd>b</kbd> and <kbd>c</kbd> keys and be done with it.  However, a)
not all programs offer this flexibility, b) sometimes changing
keybindings in large programs creates conflicts that you don&rsquo;t
anticipate (this is a simple example, so it&rsquo;s not a big problem here),
and b) controls on some devices send input events that are beyond the
normal range of a standard keyboard and might not be recognized by the
software.  Not to mention, the LEDs will just remain lifeless.
</p>
<p>So, we&rsquo;ll stick with the default keybindings in Ardour for the
buttons: the <kbd>HOME</kbd>/<kbd>END</kbd> keys to move the current playing
position to the start/end of the song and <kbd>SPC</kbd> to play/pause.
Scrolling the editor is not initially bound to any keys, so we&rsquo;ll bind
them (in Ardour) to <kbd>Shift-{</kbd> and <kbd>Shift-}</kbd>.
</p>
<p>With all that in mind, here is our AK1 configuration in Librekontrol:
</p>
<div class="example">
<pre class="example">(use-modules (librekontrol core)
             (librekontrol device)
             (librekontrol input)
             (librekontrol alsa)
             ((librekontrol devices ni-ak1) #:prefix ak1:))

(configure-device
 #:alsa-name ak1:alsa-name
 #:input-name ak1:input-name
 #:remap-events (list (make-input-event ev-key key-home)
                      (make-input-event ev-key key-end)
                      (make-input-event ev-key key-space)
                      (make-input-event ev-key key-leftbrace)
                      (make-input-event ev-key key-rightbrace)
                      (make-input-event ev-key key-leftshift))
 #:controls (list (list ak1:left remap-button
                        #:event-code key-home)
                  (list ak1:middle (make-remap-button-toggle)
                        #:event-code key-space)
                  (list ak1:right remap-button
                        #:event-code key-end)
                  (list ak1:knob (make-abs-knob-to-button)
                        #:neg-event-code key-leftbrace
                        #:pos-event-code key-rightbrace
                        #:neg-mod-keys (list key-leftshift)
                        #:pos-mod-keys (list key-leftshift)
                        #:knob-max (ak1:knob-max)
                        #:invert #t))
 #:init (list (lambda ()
                (turn-off-ctls
                 (map control-alsa-ctl
                      (list ak1:left ak1:middle ak1:right ak1:knob)))))
 #:exit-hooks (list (lambda ()
                      (turn-off-ctls
                       (map control-alsa-ctl
                            (list ak1:left ak1:middle ak1:right ak1:knob))))))
</pre></div>

<span id="index-High_002dlevel-configuration"></span>
<p>This is an example of Librekontrol&rsquo;s <em>high-level</em> configuration.
The eventual goal is to make configuring a device to do common tasks
possible without having to think (much) about the fact that you&rsquo;re
actually configuring it within a programming language.  We&rsquo;re not
quite there yet, but this is much simpler than using the underlying,
<em>low-level</em> programming interface.
</p>
<span id="index-Initialization-script"></span>
<p>You would stick that code in your Librekontrol initialization script,
which is usually located at
<samp>$HOME/.config/librekontrol/init.scm</samp>, and then you would run
the <code>librekontrol</code> command to start using the device.
See <a href="Invoking-Librekontrol.html">Invoking Librekontrol</a>.
</p>
<p>We&rsquo;ll take a look at this example in chunks in the following sections:
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Modules.html" accesskey="1">Example Config Modules</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Device.html" accesskey="2">Example Config Device</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Device-Names.html" accesskey="3">Example Config Device Names</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Remap-Events.html" accesskey="4">Example Config Remap Events</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Controls.html" accesskey="5">Example Config Controls</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Init.html" accesskey="6">Example Config Init</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Example-Config-Exit-Hooks.html" accesskey="7">Example Config Exit Hooks</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT2" href="#DOCF2">(2)</a></h3>
<p>Actually, some
system configurations suppress these events, so you might not see
these results yourself</p>
</div>
<hr>
<div class="header">
<p>
Previous: <a href="Installing-Librekontrol.html" accesskey="p" rel="prev">Installing Librekontrol</a>, Up: <a href="Getting-Started.html" accesskey="u" rel="up">Getting Started</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
