<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Librekontrol - A programmable controller editor (version
0.2, updated 3 April 2020).


Copyright (C) 2019, 2020  Brandon Invergo

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

A copy of the license is also available from the Free Software
Foundation Web site at https://www.gnu.org/licenses/fdl.html.


The document was typeset with
http://www.texinfo.org/ (GNU Texinfo).
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Example Config Controls (Librekontrol)</title>

<meta name="description" content="Example Config Controls (Librekontrol)">
<meta name="keywords" content="Example Config Controls (Librekontrol)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Tutorial.html" rel="up" title="Tutorial">
<link href="Example-Config-Init.html" rel="next" title="Example Config Init">
<link href="Example-Config-Remap-Events.html" rel="prev" title="Example Config Remap Events">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../css/main.css">


</head>

<body lang="en">
<span id="Example-Config-Controls"></span><div class="header">
<p>
Next: <a href="Example-Config-Init.html" accesskey="n" rel="next">Example Config Init</a>, Previous: <a href="Example-Config-Remap-Events.html" accesskey="p" rel="prev">Example Config Remap Events</a>, Up: <a href="Tutorial.html" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Example-Configuration_003a-Controls"></span><h4 class="subsection">2.2.5 Example Configuration: Controls</h4>

<span id="index-Controls"></span>
<p>Now we arrive at the actual definition of the device controls:
</p>
<div class="example">
<pre class="example">(configure-device
 ...
 #:controls (list (list ak1:left remap-button
                        #:event-code key-home)
                  (list ak1:middle (make-remap-button-toggle)
                        #:event-code key-space)
                  (list ak1:right remap-button
                        #:event-code key-end)
                  (list ak1:knob (make-abs-knob-to-button)
                        #:neg-event-code key-leftbrace
                        #:pos-event-code key-rightbrace
                        #:neg-mod-keys (list key-leftshift)
                        #:pos-mod-keys (list key-leftshift)
                        #:knob-max (ak1:knob-max)
                        #:invert #t))
 ...)
</pre></div>

<p>The <var>#:controls</var> option takes a list of controls.  Each
 control is, in turn, a list consisting of the control definition, the
 name of a <em>callback function</em> to be called when the control&rsquo;s
 value changes (button pressed, knob twiddled, etc.), and a number of
 optional arguments to be passed to the callback function.  Optional
 arguments can simply be omitted if you&rsquo;re happy with the default behavior.
</p>
<p>The AK1&rsquo;s controls have all already been defined in
&lsquo;<samp>(librekontrol devices ni-ak1)</samp>&rsquo; and are bound to the symbols
<var>ak1:left</var>, <var>ak1:middle</var>, <var>ak1:right</var> and <var>ak1:knob</var>.
Each control is defined as having an associated input event and,
optionally, an ALSA control (usually an LED).  If you are configuring
a new device, you will need to define the controls yourself via the
<var>make-control</var> procedure.
</p>
<span id="index-Callback-functions"></span>
<span id="index-Connecting-controls"></span>
<p>Each control is <em>connected</em> to the listed callback function.  When
Librekontrol detects an input event originating from that control, it
runs the callback function.  Using the high-level configuration,
currently only one callback function can be associated with an input
event.  If you want to call a series of procedures when an event
occurs, you must use the low-level API to add event <em>hooks</em>.
</p>
<div class="example">
<pre class="example">(configure-device
 ...
 #:controls (list (list ak1:left remap-button
                        #:event-code key-home)
                  ...
                  (list ak1:right remap-button
                        #:event-code key-end)
                   ...)
 ...)
</pre></div>

<p>We see a few different callbacks here.  <var>ak1:left</var> and
<var>ak1:right</var> are both connected to the <var>remap-button</var> callback.
This callback simply generates a new input event that sends a
different event code as specified by the <var>#:event-code</var> option
(<kbd>HOME</kbd> and <kbd>END</kbd>, respectively).  When the button is pressed
in, the associated LED turns on, and when it is released, the LED
turns off.
</p>
<div class="example">
<pre class="example">(configure-device
 ...
 #:controls (list ...
                  (list ak1:middle (make-remap-button-toggle)
                        #:event-code key-space)
                  ...)
 ...)
</pre></div>

<p><var>ak1:middle</var> is connected to something that looks fairly similar,
however it&rsquo;s wrapped in parentheses:
&lsquo;<samp>(make-remap-button-toggle)</samp>&rsquo;.  What&rsquo;s going on here?  Because
it&rsquo;s wrapped in parentheses, Guile interprets this as a call to a
procedure, which it is.  The <var>make-remap-button-toggle</var> procedure
actually returns a new callback function.  We have to do this because
we want the middle button to behave like a toggle switch: press it
once to send the <kbd>SPC</kbd> event and turn on its LED; press it a
second time to send the <kbd>SPC</kbd> event again and turn off its LED.
We need a way to remember from one press to another whether the toggle
is &ldquo;on&rdquo; or &ldquo;off&rdquo;.  One way to do this would be to require the user
to maintain a variable that stores the current state and to pass it to
the procedure as an argument.  Another way is to take advantage of
something called a <em>lexical closure</em> (this is beyond the scope of
this tutorial but see <a href="https://www.gnu.org/software/guile/manual/html_node/About-Closure.html#About-Closure">(guile)About
Closure</a>), but this requires us to create a new function for
each control whose state we want to remember.
</p>
<div class="example">
<pre class="example">(configure-device
 ...
 #:controls (list ...
                  (list ak1:knob (make-abs-knob-to-button)
                        #:neg-event-code key-leftbrace
                        #:pos-event-code key-rightbrace
                        #:neg-mod-keys (list key-leftshift)
                        #:pos-mod-keys (list key-leftshift)
                        #:knob-max (ak1:knob-max)
                        #:invert #t))
 ...)
</pre></div>

<p>Finally, <var>ak1:knob</var> is connected to
&lsquo;<samp>(make-abs-knob-to-button)</samp>&rsquo;.  Once again, this is a call to a
procedure that returns a callback function.  This one converts an
absolute-position control like a knob and remaps it to keypresses.
Here the state that we need to remember is related to determining
whether the knob&rsquo;s position is increasing or decreasing.  We then
associate a keypress event to negative events (rotating
counter-clockwise; <var>#:neg-event-code</var>) and one to positive events
(clockwise; <var>#:pos-event-code</var>).  We also can specify a list of
modifier keys (<kbd>Shift</kbd>, <kbd>Ctrl</kbd>, etc.) that we want to apply to
the new keypress events (<var>#:neg-mod-keys</var>/<var>#:pos-mod-keys</var>;
note that we could have done the same for any of our earlier
<var>remap-button</var> callbacks).
</p>
<p>We must specify the maximum value that the original input event can
generate, which is stored in a <em>parameter</em> <var>ak1:knob-max</var>,
defined in &lsquo;<samp>(librekontrol devices ni-ak1)</samp>&rsquo;.  In general, such
maximum values are defined as parameters, the values of which can be
retrieved by calling them like procedures
(e.g. &lsquo;<samp>(ak1:knob-max)</samp>&rsquo;).  See <a href="https://www.gnu.org/software/guile/manual/html_node/Parameters.html#Parameters">(guile)Parameters</a> for more
information on how you might take advantage of that.  For now, though,
we just want to take the defined maximum value (which happens to be
999, if you&rsquo;re curious).
</p>
<p>The last option to <var>ak1:knob</var> is &lsquo;<samp>#:invert #t</samp>&rsquo;.  This sets
the <var>invert</var> option to &ldquo;true&rdquo;.  This is necessary because the
AK1&rsquo;s behavior is not what one would intuitively expect: rotating the
knob clockwise <em>decreases</em> the absolute position.  Setting
<var>#:invert</var> makes the position increase with clockwise rotation as
expected.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Example-Config-Init.html" accesskey="n" rel="next">Example Config Init</a>, Previous: <a href="Example-Config-Remap-Events.html" accesskey="p" rel="prev">Example Config Remap Events</a>, Up: <a href="Tutorial.html" accesskey="u" rel="up">Tutorial</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
