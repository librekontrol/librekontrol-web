<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Librekontrol - A programmable controller editor (version
0.2, updated 3 April 2020).


Copyright (C) 2019, 2020  Brandon Invergo

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

A copy of the license is also available from the Free Software
Foundation Web site at https://www.gnu.org/licenses/fdl.html.


The document was typeset with
http://www.texinfo.org/ (GNU Texinfo).
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>New Devices (Librekontrol)</title>

<meta name="description" content="New Devices (Librekontrol)">
<meta name="keywords" content="New Devices (Librekontrol)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Reference.html" rel="next" title="Reference">
<link href="Invoking-Librekontrol.html" rel="prev" title="Invoking Librekontrol">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../css/main.css">


</head>

<body lang="en">
<span id="New-Devices"></span><div class="header">
<p>
Next: <a href="Reference.html" accesskey="n" rel="next">Reference</a>, Previous: <a href="Invoking-Librekontrol.html" accesskey="p" rel="prev">Invoking Librekontrol</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Defining-a-New-Device"></span><h2 class="chapter">4 Defining a New Device</h2>

<p>If you want to use Librekontrol to configure a device, particularly if
you want to do this via the high-level configuration commands, you
will want to fully define the device in advance.  This can be done
within your initialization script, or you can put the definitions in a
new module, which you can then load from your initialization script
(see <a href="https://www.gnu.org/software/guile/manual/html_node/Modules.html#Modules">(guile)Modules</a>).
</p>
<span id="index-How-to-help-2"></span>
<span id="index-Contributing-2"></span>
<p>If you define a new device module, please consider contributing it to
Librekontrol so that other owners of the device may use the
definitions.
</p>
<span id="index-Devices-1"></span>
<p>Defining a device consists of the following steps:
</p>
<ol>
<li> <span id="index-Device-names-1"></span>
Determine the device&rsquo;s input and ALSA names (if any).

<p>You might find the input name by using the <code>evtest</code>.  The
input device name is listed on the right.  If it&rsquo;s not clear which
name refers to your device, you can use <code>evtest</code> to test each
potential candidate until you find the right one.  Alternatively, you
can read the kernel source code.
</p>
<p>You can find the ALSA name using the ALSA <code>aplay</code> command by
running &lsquo;<samp>aplay --list-pcms</samp>&rsquo;.  Several devices might be listed so
you will have to determine which one refers to the device you would
like to configure.  Use the name directly following the bit that says
&lsquo;<samp>CARD=</samp>&rsquo; (nevermind if the device isn&rsquo;t actually a sound card).
</p>
<p>By convention, you would bind these values to <var>input-name</var> and
<var>alsa-name</var>, respectively.
</p>
</li><li> <span id="index-Input-events"></span>
Deduce and define the input events associated with each input control,
if any.

<p>The easiest way to do this is to open the device (after you&rsquo;ve
determined its input name; see <a href="Low_002dLevel-Devices.html">Low-Level Devices</a>) in your
initialization script and then run <code>librekontrol</code> with the
<samp>--debug</samp> option.  See <a href="Invoking-Librekontrol.html">Invoking Librekontrol</a>.  Start
interacting with the device.
</p>
<p>So, your <samp>init.scm</samp> looks like this:
</p>
<div class="example">
<pre class="example">(use-modules (librekontrol core))

(define input-name &quot;My Device Name&quot;)

(open-device #f input-name #f #f)
</pre></div>

<p>After running the program and pressing a button on the device, the
output will look something like this:
</p>
<div class="example">
<pre class="example">EV_KEY event: KEY_A     '(1 . 30)       1
EV_KEY event: KEY_A     '(1 . 30)       0
</pre></div>

<p>This tells us that hitting that button sends a keypress event
(<var>ev-key</var>) corresponding to hitting the <kbd>a</kbd> key (<var>key-a</var>).
See <a href="Input-Events.html">Input Events</a>.  These correspond to event type 1 and event code
30, although you probably don&rsquo;t need these numeric values unless your
device is sending undefined events (which is possible).  The final
number just tells you the value of the event: 1 means the button is
pressed and 0 means it&rsquo;s released.  The value might be useful for
understanding absolute or relative-position events.
</p>
<p>You would then define this event via the <var>define-input-event</var>
syntax from &lsquo;<samp>(librekontrol input)</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">(define-input-event my-button ev-key key-a)
</pre></div>

<p>For absolute-position events (<var>ev-abs</var>), you will want also to
define the minimum and maximum possible values.  You can either read
the kernel code to find this or you can use the <var>abs-input-max</var>
procedure in &lsquo;<samp>(librekontrol core)</samp>&rsquo;.  See <a href="Low_002dLevel-Input-Events.html">Low-Level Input Events</a>.  By convention, you should define variables holding maximum
values using <var>make-input-max-parameter</var> from &lsquo;<samp>(librekontrol
device)</samp>&rsquo; (TODO: there should be a <var>make-input-min-parameter</var>):
</p>
<div class="example">
<pre class="example">(define my-slider-max (make-input-max-parameter 999))
</pre></div>

</li><li> <span id="index-ALSA-hardware-controls"></span>
Deduce and define the ALSA hardware control IDs associated with each
LED (or other feature), if any.

<p>You can find these via the command <code>amixer</code> by running
&lsquo;<samp>amixer controls</samp>&rsquo; (note that you might have to select the device
using the <samp>-c</samp>/<samp>--card</samp> option.  Define the controls
using <var>define-alsa-ctl</var> from &lsquo;<samp>(librekontrol alsa)</samp>&rsquo;.
</p>
<p><code>amixer</code> will list &lsquo;<samp>numid=N</samp>&rsquo; next to each control; this
is the numeric id (<var>numid</var>) to pass to <var>define-alsa-ctl</var> as the
first argument.  The second argument should be <var>&rsquo;boolean</var> (i.e. it
can be either on or off) or <var>&rsquo;integer</var> (it can take a range of
values, e.g. an LED that can have a range of brightnesses).
</p>
<div class="example">
<pre class="example">(define-alsa-ctl my-led 1 'boolean)
</pre></div>

</li><li> <span id="index-Controls-1"></span>
<span id="index-High_002dlevel-configuration-2"></span>
Define high-level controls that are compatible with the
callback-function system.  These consist of an input event and,
optionally, an associated ALSA hardware control.

<p>This is pretty straight-forward.  Just use <var>define-control</var> from
&lsquo;<samp>(librekontrol device)</samp>&rsquo;.  The first argument is the input event
(from <var>define-input-event</var>) associated with the control and the
second argument is the ALSA hardware control (from
<var>define-alsa-ctl</var>, or &lsquo;<samp>#f</samp>&rsquo; if there is no associated ALSA
ctl).
</p>
<div class="example">
<pre class="example">(define-control button my-button my-led)
(define-control slider my-slider #f)
</pre></div>
</li></ol>

<hr>
<div class="header">
<p>
Next: <a href="Reference.html" accesskey="n" rel="next">Reference</a>, Previous: <a href="Invoking-Librekontrol.html" accesskey="p" rel="prev">Invoking Librekontrol</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
