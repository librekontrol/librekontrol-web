;; Copyright (C) 2019, 2020, 2021  Brandon Invergo
;; Copyright (C) 2016  David Thompson

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (haunt html)
             (haunt page)
             (haunt post)
             (haunt site)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader commonmark)
             (ice-9 match)
             (srfi srfi-19)
             (web uri))

(define %releases
  '("0.2.1" "0.2" "0.1"))

(define (tarball-url version)
  (string-append "http://www.librekontrol.org/files/librekontrol-"
                 version ".tar.gz"))

(define (tarball-sig-url version)
  (string-append "http://www.librekontrol.org/files/librekontrol-"
                 version ".tar.gz.sig"))

(define %download-button
  (match %releases
    ((version . _)
     `(a (@ (class "btn btn-primary btn-lg")
            (role "button")
            (href ,(tarball-url version)))
         "Download Librekontrol " ,version))))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define (anchor content uri)
  `(a (@ (href ,uri)) ,content))

(define (logo src)
  `(img (@ (class "logo") (src ,(string-append "/images/" src)))))

(define (jumbotron content)
  `(div (@ (class "jumbotron"))
        (div (@ (class "row"))
             (div (@ (class "column-logo"))
                  (img (@ (class "big-logo")
                          (src "/images/librekontrol-logo.png"))))
             (div (@ (class "column-info")) ,content))))

(define librekontrol-layout
  (lambda (site title body)
    `((doctype "html")
      (head
       (meta (@ (charset "utf-8")))
       (title ,(string-append title " — " (site-title site)))
       ,(stylesheet "main")
       (body
        (header (@ (class "navbar"))
                (div (@ (class "container"))
                     (ul
                      (li ,(anchor "Home" "/"))
                      (li ,(anchor "Supported Devices" "/supported-devices.html"))
                      (li ,(anchor "Documentation" "/manual/index.html"))
                      (li ,(anchor "Download" "/download.html")))))
        (dif (@ (class "container"))
             ,body
             (footer (@ (class "text-center"))
                     (p (small "Copyright © 2019, 2020 Brandon Invergo | "
                               "This website was built with "
                               (a (@ (href "http://haunt.dthompson.us")) "Haunt")
                               " | "
                               (a (@ (href "https://github.com/MetroWind/lab-theme")) "Lab Color Theme")
                               " Copyright © 2010—2018 MetroWind")))))))))

(define librekontrol-post-template
  (lambda (post)
    `((h2 ,(post-ref post 'title))
      (h3 "by " ,(post-ref post 'author)
          " (" ,(date->string* (post-date post))
          ")")
      (div ,(post-sxml post)))))

(define librekontrol-collection-template
  (lambda (site title posts prefix)
    (define (post-uri post)
      (string-append "/" (or prefix "")
                     (site-post-slug site post) ".html"))
    `(,(jumbotron
        `((p "Librekontrol is a programmable controller editor for
GNU/Linux.  You use Librekontrol to (re-)define the events that occur
when you interact with a controller: keyboards, gamepads, music
controllers, etc.") ,%download-button))
      (p "In fact, music controllers are a primary focus of
Librekontrol.  As such, it also provides access to hardware features
defined through ALSA (Advanced Linux Sound Architecture), typically
LEDs on the device.  Librekontrol can also create software MIDI ports
through ALSA for any configured controller.")
      (p "Rather than simply offering a basic configuration system,
Librekontrol is fully programmable through "
         (a (@ (href "https://www.gnu.org/software/guile")) "Guile") ",
a dialect of the programming language Scheme." )
      (p "This means that the sky is the limit with what you can do
with Librekontrol: conveniently map the buttons on your device to
keyboard shortcuts configured in a program, compose complex keyboard
macros, or use your gamepad as a MIDI controller.")
      (h2 "News")
      (ul
       ,@(map (lambda (post)
                `(li
                  (a (@ (href ,(post-uri post)))
                     ,(post-ref post 'title)
                     " ("
                     ,(date->string* (post-date post)) ")")))
              (posts/reverse-chronological posts)))
      (h2 "Contributing")
      (p "Development occurs
at " (a (@ (href "https://www.gitlab.com/librekontrol/librekontrol")) "Gitlab")
".  Please report all bugs and feature requests there.")
      (p "Contributions to Librekontrol are most welcome.  In addition
to fixing bugs or improving performance, please consider contributing
new device definitions and example configurations.  Of course, new
functionality such as new event callback functions would be great.")
      (h2 "License")
      (p "Librekontrol is "
         (a (@ (href "https://www.gnu.org/philosophy/free-sw.html"))
            "Free Software")
         " available under the "
         (a (@ (href "https://www.gnu.org/licenses/gpl.html"))
            "GNU General Public License")
         " version 3 or later.  There is NO WARRANTY, to the extent
permitted by law.")
)))

(define librekontrol-theme
  (theme #:name "Librekontrol"
         #:layout librekontrol-layout
         #:post-template librekontrol-post-template
         #:collection-template librekontrol-collection-template))

(define (download-page site posts)
  (define body
    `((h2 "Download")
      (table (@ (class "table"))
       (thead
        (tr (th "Source") (th "GPG signature")))
       (tbody
        ,(map (match-lambda
                (version
                 (let ((tarball-name (string-append "librekontrol-" version ".tar.gz")))
                   `(tr
                     (td (a (@ (href ,(tarball-url version))) ,tarball-name))
                     (td ,`(a (@ (href ,(tarball-sig-url version)))
                              ,(string-append tarball-name ".sig"))
                         "")))))
              %releases)))))
  (make-page "download.html"
             (with-layout librekontrol-theme site "Download" body)
             sxml->html))

(define (supported-dev-page site posts)
  (define body
    '((h2 "Supported Devices")
      (p "Technically, you can use Librekontrol to configure any
device that is recognized by the kernel as an input device and/or as
having hardware controls (from the computer's perspective, such as
LEDs) that can be manipulated via ALSA.")
      (p "Librekontrol officialy \"supports\" a number of devices.
This means that the details of these devices are fully specified in
the Librekontrol code.  Thus, you can use these devices
\"out-of-the-box\" via a high-level configuration interface.")
      (p "The following devices are presently supported (or nearly
supported) by Librekontrol:")
      (p
       (table (@ (class "table"))
              (thead
               (tr
                (th "Manufacturer")
                (th "Device")
                (th "Definitions")
                (th "High-level Config.")
                (th "Example Configs.")))
              (tbody 
               (tr
                (td "Native Instruments")
                (td "Audio 4 DJ")
                (td "complete")
                (td "yes")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Audio 8 DJ")
                (td "complete")
                (td "yes")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Audio Kontrol 1")
                (td "complete")
                (td "yes")
                (td "yes"))
               (tr
                (td "Native Instruments")
                (td "Kontrol S4")
                (td "incomplete")
                (td "no")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Kontrol X1")
                (td "incomplete")
                (td "no")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Kore")
                (td "complete")
                (td "no")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Kore 2")
                (td "complete")
                (td "no")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Maschine" (sup "*"))
                (td "complete")
                (td "yes")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Rig Kontrol 2")
                (td "complete")
                (td "yes")
                (td "no"))
               (tr
                (td "Native Instruments")
                (td "Rig Kontrol 3")
                (td "complete")
                (td "yes")
                (td "no")))))
      (p (sup "*") "Note: The LCD displays on the Native Instruments
Maschine are not exposed to the kernel and thus are not able to be
used via Librekontrol")
      (p "Some devices are marked as having \"incomplete\"
definitions.  For these devices, all of the input events that they can
produce are known, however it is not clear which control each event
maps to.  Devices without support for high-level configuration lack
couplings between input controls and LEDs (that is, it's not always
clear which LED should be turned on with which control).  If you own
any of these devices and want to help complete their support, please
get in touch.")
      (p "To use other devices, you must first define all of their
inputs manually or otherwise use the low-level programming interface.
Please consider offering these configurations to the Librekontrol
developers to be included in future releases of the software.")
      (p "We intend to have example configurations available for all
supported devices, however designing a configuration requires access
to and usage of the device in question.  If you have created a
generally useful configuration for a supported device, please consider
offering it for inclusion in Librekontrol as an example.")))
  (make-page "supported-devices.html"
             (with-layout librekontrol-theme site "Supported Devices" body)
             sxml->html))

(define %collections
  `(("Home" "index.html" ,posts/reverse-chronological)))

(site #:title "Librekontrol"
      #:domain "librekontrol.org"
      #:default-metadata
      '((author . "Brandon Invergo")
        (email  . "brandon@invergo.net"))
      #:readers (list commonmark-reader)
      #:builders (list (blog #:theme librekontrol-theme
                             #:collections %collections)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       download-page
                       supported-dev-page
                       (static-directory "images")
                       (static-directory "css")
                       (static-directory "manual")
                       (static-directory "files")))
