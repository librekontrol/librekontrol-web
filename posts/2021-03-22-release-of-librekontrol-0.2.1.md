title: Release of Librekontrol 0.2.1
date: 2021-03-22 10:16
tags: release
summary: A quick bugfix/performance release
---

I'm happy to announce a new release of Librekontrol!
Librekontrol is a programmable controller editor for GNU/Linux.  This
is primarily a bugfix release, but a couple new features made their
way in.

Please note that this should be considered *alpha* software.  Assume
that you might encounter bugs and that the software might fail
spectacularly if you try to do something particularly clever with it.
It's certainly ready to be tested and played around with, and indeed,
you can already start using it regularly for everyday projects.

Note that the code also hasn't seen any real optimization effort, so
latency will not be ideal if you're using it to configure a device
that you will play in real-time.

Of course, all of these problems would make great entry points into
helping out with [Librekontrol's
development](https://www.gitlab.com/librekontrol/librekontrol).  Find
a bug, zap it, and share your fix for the greater good!

Please also consider helping out by completing the existing device
definitions if you own any of them (all devices from [Native
Instruments](https://www.native-instruments.com) and by contributing
new device definitions.

[Download Librekontrol
0.2.1](http://www.librekontrol.org/files/librekontrol-0.2.1.tar.gz)
[[sig](http://www.librekontrol.org/files/librekontrol-0.2.1.tar.gz.sig)]

## What's new in 0.2.1

### Bug fixes

#### Fix a Guile compilation error

An apparently inconsequential (but nevertheless annoying) compilation
error was resolved.

#### Improve performance when making dynamic changes to an ALSA control

Updating LED brightness values for velocity sensitive pads on the
Maschine leads to a perceived slowdown.  The ALSA function
`snd_ctl_elem_write` appears to be the culprit: new EV_ABS (absolute)
events are generated before the write operation (updating the LED
state) can complete, leading to a queue of writes.  Since, e.g for the
Maschine, this involves rescaling input values (0 to 4096) to the LED
range of 0 to 63, we are potentially writing the same value
repeatedly.  By filtering out same-value writes, we can significantly
reduce the number of writes and thereby increase perceived
performance.

