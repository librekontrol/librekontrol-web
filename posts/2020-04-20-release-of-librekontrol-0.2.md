title: Release of Librekontrol 0.2
date: 2020-04-20 22:48
tags: release
summary: The second release of Librekontrol!
---

I'm happy to announce the second release of Librekontrol!
Librekontrol is a programmable controller editor for GNU/Linux.  This
is primarily a bugfix release, but a couple new features made their
way in.

Please note that this should be considered *alpha* software.  Assume
that you might encounter bugs and that the software might fail
spectacularly if you try to do something particularly clever with it.
It's certainly ready to be tested and played around with, and indeed,
you can already start using it regularly for everyday projects.  You
probably shouldn't depend on it for your big gig at the warehouse rave
next weekend, though.

Note that the code also hasn't seen any real optimization effort, so
latency will not be ideal if you're using it to configure a device
that you will play in real-time.

Of course, all of these problems would make great entry points into
helping out with [Librekontrol's
development](https://www.gitlab.com/librekontrol/librekontrol).  Find
a bug, zap it, and share your fix for the greater good!

Please also consider helping out by completing the existing device
definitions if you own any of them (all devices from [Native
Instruments](https://www.native-instruments.com) and by contributing
new device definitions.

[Download Librekontrol
0.2](http://www.librekontrol.org/files/librekontrol-0.2.tar.gz)
[[sig](http://www.librekontrol.org/files/librekontrol-0.2.tar.gz.sig)]

## What's new in 0.2

### New Features

#### New make-finite-midi-control-abs-knob procedure

The `make-finite-midi-control-abs-knob` procedure generates a callback
function that helps to make an infinite rotary encoder function as a
finite MIDI control knob.  That is, if you have a knob on your device
that can rotate infinitely in either direction, a callback produced by
this procedure will ensure that no matter how much you rotate the
knob, say, right, the value will stop at its maximum.  Normally, these
knobs would "loop around" and go from the max value back down to zero
before increasing again.

#### System process-related callback functions take a command-generating procedure

This is a breaking change!  Your config will need to be updated.

Previously, all callback functions that spawn system processes
(e.g. `system-button` or `system-abs-knob`) would take a command as a
simple string (e.g. `"echo 'hello'"`).  For knob-based callback
functions, where this command is passed the knob's value, this was
inflexible.  Those callback functions now take an argument `cmdproc`
which takes a procedure as its value.  The procedure takes a single
argument (the knob's value) and returns a string representing the
system command to call.  See the example in
`examples/ni-ak1-playerctl.scm` for an illustration of how this is
useful.

#### reset-idle-hook and reset-exit-hook take only 1 argument

Previously, these procedures required specifying an event ID, which of
course isn't necessary for resetting device-wide hooks.

#### The idle hook is cleared before running exit hooks

This avoids the possibility where the idle hook is triggered before
all of the exit hooks have finished.

#### Remap event failures generate more useful error messages

This might be helpful for troubleshooting if permissions are not set
up properly for Librekontrol.

### Bug fixes

#### Trying to set or read the ALSA Ctl of a control without an associated Ctl no longer fails
Some safety nets have been put into place to make sure that controls
without associated ALSA Ctls are not manipulated as if they do have Ctls.

#### Trying to add more procedures to an hook after the initial setup no longer fails

#### Init procedures now work properly

#### Native Instruments Rig Kontrol 3 device definitions are now correct

Thanks to user fratto, the device definitions (Ctls, input, controls)
for the NI Rig Kontrol 3 are now correct and more accurately named.  

#### Native Instruments Rig Kontrol 2 device definitions are now better

ALSA Ctl definitions for the NI Rig Kontrol 2 were corrected and some
of the inputs have been renamed following the RK3 fixes.  However, the
labeling of some Ctls / inputs may be incorrect still (e.g. in the
RK3, the LED that the Linux kernel driver calls "LED 1" is actually
"LED 5" on the device).  If you have an RK2 and you can help resolve
this once and for all, please get in touch.
