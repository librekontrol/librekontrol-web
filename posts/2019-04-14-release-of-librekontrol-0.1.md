title: Release of Librekontrol 0.1
date: 2019-04-14 22:48
tags: release
summary: The first release of Librekontrol!
---

I'm happy to announce the inaugural release of Librekontrol!
Librekontrol is a programmable controller editor for GNU/Linux.  This
first release sees the software in a usable state, with most basic
functionality present for everyday device configuration.

Please note that this should be considered *alpha* software.  Assume
that you might encounter bugs and that the software might fail
spectacularly if you try to do something particularly clever with it.
It's certainly ready to be tested and played around with, and indeed,
you can already start using it regularly for everyday projects.  You
probably shouldn't depend on it for your big gig at the warehouse rave
next weekend, though.

Note that the code also hasn't seen any real optimization effort, so
latency will not be ideal if you're using it to configure a device
that you will play in real-time.

Of course, all of these problems would make great entry points into
helping out with [Librekontrol's
development](https://www.gitlab.com/librekontrol/librekontrol).  Find
a bug, zap it, and share your fix for the greater good!

Please also consider helping out by completing the existing device
definitions if you own any of them (all devices from [Native
Instruments](https://www.native-instruments.com) and by contributing
new device definitions.

[Download Librekontrol
0.1](http://www.librekontrol.org/files/librekontrol-0.1.tar.gz)
[[sig](http://www.librekontrol.org/files/librekontrol-0.1.tar.gz.sig)]
